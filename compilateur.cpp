#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char current, lookedAhead;                // Current char    
int NLookedAhead=0;

void ReadChar(void){
    if(NLookedAhead>0){
        current=lookedAhead;    // Char has already been read
        NLookedAhead--;
    }
    else
        // Read character and skip spaces until 
        // non space character is read
        while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void){
    while(cin.get(lookedAhead) && (lookedAhead==' '||lookedAhead=='\t'||lookedAhead=='\n'));
    NLookedAhead++;
}

void Error(string s) {
    cerr << s << endl;
    exit(-1);
}

void ArithmeticExpression(void);
void AdditiveOperator(void);
void Term(void);

void AdditiveOperator(void) {
    if (current == '+' || current == '-')
        ReadChar();
    else
        Error("Opérateur additif attendu");
}

void Digit(void) {
    if (current < '0' || current > '9')
        Error("Chiffre attendu");
    else {
        cout << "\tpush $" << current << endl;
        ReadChar();
    }
}

void Term(void) {
    if (current == '(') {
        ReadChar();
        ArithmeticExpression();
        if (current != ')')
            Error("')' était attendu");
        else
            ReadChar();
    } else if (current >= '0' && current <= '9') {
        Digit();
    } else {
		cout << current << endl ; 
        Error("'(' ou chiffre attendu");
    }
}

void ArithmeticExpression(void) {
    char adop;
    Term();
    
    while (current == '+' || current == '-') {
        adop = current;
        AdditiveOperator();
        Term();
        cout << "\tpop %rbx" << endl; // Récupérer le premier opérande
        cout << "\tpop %rax" << endl; // Récupérer le deuxième opérande
        if (adop == '+')
            cout << "\taddq %rbx, %rax" << endl; // Ajouter les deux opérandes
        else
            cout << "\tsubq %rbx, %rax" << endl; // Soustraire les deux opérandes
        cout << "\tpush %rax" << endl; // Stocker le résultat
    }
}

void Expression(void) {
    char al = 'a'; // Suppose que al est un caractère
    ArithmeticExpression();
    LookAhead(); 
    std::string rlop;
    if (current == '>' && lookedAhead == '=') {
        rlop = ">=";
    } else if (current == '<' && lookedAhead == '=') {
        rlop = "<=";
    } else if (current == '=' && lookedAhead == '=') {
        rlop = "==";
    } else if (current == '<' && lookedAhead == '>') {
        rlop = "<>";
    } else if (current == '>') {
        rlop = ">";
    } else if (current == '<') {
        rlop = "<";
    } else if (current == '=') {
        rlop = "=";
    } else {
        Error("Opérateur de comparaison non reconnu");
    }
     
    ReadChar(); // Avance pour lire le prochain caractère après les deux caractères de l'opérateur de comparaison
    ReadChar(); // (i.e., les deux caractères que nous avons déjà pris en compte)
    
    ArithmeticExpression();

    cout << "\tpop %rcx" << endl;
    cout << "\tpop %rbx" << endl;
    cout << "\tcmp %rcx, %rbx" << endl;
    
    if (rlop == ">=") {
        cout << "\tjge vrai" << al << endl;
    } else if (rlop == "<=") {
        cout << "\tjle vrai" << al << endl;
    } else if (rlop == "==") {
        cout << "\tje vrai" << al << endl;
    } else if (rlop == "<>") {
        cout << "\tjne vrai" << al << endl;
    } else if (rlop == ">") {
        cout << "\tjg vrai" << endl;
    } else if (rlop == "<") {
        cout << "\tjl %" << al << endl;
    }
       cout << "\tpush $0 " << endl ;
       cout << "\tjmp prochain " << endl  ;  
      cout << "vrai: push $1 " << endl ; 
      cout << "prochain:" << endl ; 
      
}

int main(void) {
    // En-tête pour l'assembleur / linker gcc
    cout << "\t\t\t# Ce code a été produit par le compilateur CERI" << endl;
    cout << "\t.text\t\t# Les lignes suivantes contiennent le programme" << endl;
    cout << "\t.globl main\t# La fonction main doit être visible depuis l'exterieur" << endl;
    cout << "main:\t\t\t# Corps de la fonction main :" << endl;
    cout << "\tmovq %rsp, %rbp\t# Sauvegarder la position du sommet de la pile" << endl;

    // Procéder à l'analyse et à la production de code
    ReadChar();
    Expression();
    ReadChar();

    // Pied pour l'assembleur / linker gcc
    cout << "\tmovq %rbp, %rsp\t\t# Restaurer la position du sommet de la pile" << endl;
    cout << "\tret\t\t\t# Retour de la fonction main" << endl;

    if (cin.get(current)) {
        cerr << "Caractères en trop à la fin du programme : [" << current << "]" << endl;
        Error(".");
    }

    return 0;
}
